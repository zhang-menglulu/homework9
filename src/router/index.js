import Vue from 'vue'
import VueRouter from 'vue-router'
import CampusBinhai from '../views/CampusBinhai.vue'
import CampusChashan from '../views/CampusChashan.vue'
import CampusYueqing from '../views/CampusYueqing.vue'
import Login from '../views/Login.vue'
import StudentSystem from '../views/StudentSystem.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'binhai',
    component: CampusBinhai
  },
  {
    path: '/chashan',
    name: 'chashan',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: CampusChashan
  },
  {
    path: '/yueqing',
    name: 'yueqing',
    component: CampusYueqing
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/system',
    name: 'system',
    component: StudentSystem
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
